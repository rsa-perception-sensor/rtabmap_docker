<table align="center"><tr><td align="center" width="9999">
<img src="images/logo.png" align="center" width="150" alt="Project logo">

# RTAB-Map ROS Docker
a mapping tool designed for Trisect

<img src="images/final_result.png" align="center" width="1000" alt="Point cloud of tank interior with statue">
</td></tr></table>

[TOC]

# Introduction

[rtabmap_docker](https://gitlab.com/rsa-perception-sensor/rtabmap_docker) is a [Docker](https://docs.docker.com/get-started/overview/) image which runs [rtabmap_ros](http://wiki.ros.org/rtabmap_ros). It takes a left/ depth image pair and outputs a host of topics including a point cloud of the map it’s formed. It’s intended to provide a mapping system for the Trisect perception sensor. It can run on either the Trisect’s onboard Nvidia Jetson Xavier NX or connected Ubuntu 20.04 desktop; it can run real-time, and from previously-collected images within a rosbag.

# Installation and Requirements
## Requirements

* ROS
    * Desktop: noetic (for the `ros:noetic-ros-base` Docker base image)
    * Trisect/ Jetson: melodic (for the `dustynv/ros:melodic-ros-base-l4t-r32.5.0` Docker base image)
    * Note: other base images should work if needed for other devices
* Docker
    * Desktop: Docker version 24.0.2, build cb74dfc works
    * Trisect/ Jetson: Docker version 20.10.21, build 20.10.21-0ubuntu1~18.04.3 works
* A perception sensor, like the Trisect, which can output (at a minimum) messages of the following type. Ensure that the depth and rgb image have the same size (i.e. if one is scaled down, so should the other)
    * `depth/image` (sensor_msgs/Image): Registered depth image, e.g. `/trisect/stereo/depth`.
    * `rgb/image` (sensor_msgs/Image): RGB/Mono image. Should be rectified, e.g. `/trisect/stereo/left/scaled_image_rect`.
    * `rgb/camera_info` (sensor_msgs/CameraInfo): RGB camera metadata, e.g. `/trisect/stereo/left/scaled_camera_info`.

## Installation
Once the prerequisites have been met, run:
`git clone git@gitlab.com:rsa-perception-sensor/rtabmap_docker.git`
then \
`git submodule update --init`

### Installing rtabmapviz
If you have ROS installed, just run `sudo apt install ros-$ROS_DISTRO-rtabmap-ros`. \
Otherwise, just follow the [installation steps](https://github.com/introlab/rtabmap_ros#installation).


# Building and Running
0. Ensure that you have a roscore running, preferably on the desktop, and that the ROS_MASTER_URI is respectively set, e.g. `export ROS_MASTER_URI=porcine:11311/`

##  To visualize output
1. Run rosparam set /use_sim_time true if using input playing from a rosbag
2. Go to where rtabmap_ros is installed, such as `/opt/ros/noetic/share/rtabmap_ros/launch` and run roslaunch rtabmap.launch. This should open up an instance of rtabmapviz on your desktop.
3. Alternatively, you can view a slightly downsampled point cloud representation of the map with the `cloud_map` topic through RVIZ.


## To run build and run rtabmap_docker
1. If on desktop: `docker build -t trisect-docker -f Dockerfile.desktop --network=host . `\
If on Trisect: `docker build -t trisect-docker -f Dockerfile.jetson --network=host . `
2. If on desktop: `docker-compose -f docker-compose.desktop.yml up -d `\
If on Trisect: `docker-compose -f docker-compose.jetson.yml up -d`
3. Enter with `docker exec -it trisect-docker bash`
4. `source devel/setup.bash`
5. `rosparam set /use_sim_time true` if using bag file
6. `cd /home/catkin_ws/src/rtabmap_docker/launch` and `roslaunch rtabmap_depth.launch`. \
Given that this is running on the same local roscore, the results from this rtabmap_ros node will be visualized on the local rtabmapviz (or rviz) interface. \
Note: if you wanted to run this using only stereo image pairs as input, do `roslaunch rtabmap.launch` instead; however, with the Trisect software this is much less efficient since Trisect already efficiently calculates disparity with gpu_stereo_image_proc, so you would be doing unecessary computation.

There are two configurations: \
Configuration A:
<div align="center">
<img src="images/config_1.png" align="center" width="500" alt="First configuration">
</div>
\
Configuration B:
<div align="center">
<img src="images/config_2.png" align="center" width="500" alt="Second configuration">
</div>

## To record a bagfile for input
Use a command which includes the minimum necessary parameters (depth image, rectified image, rectified image info), such as: \
```
rosbag record /trisect/stereo/disparity /trisect/stereo/disparity/image /trisect/stereo/depth /trisect/left/camera_info /trisect/left/image_rect /trisect/left/imaging_metadata /trisect/right/camera_info /trisect/right/image_rect /trisect/right/imaging_metadata
```


## To play a bagfile
Play the trisect bag file with --clock, e.g. `rosbag play --clock no_lights_2023-05-23_slam_orbit_reprocessed.bag`. You should see log messages pop up on the terminal running the docker, and a map being constructed in rtabmapviz.

When the bag file has finished playing, kill rtabmap_ros (both docker and Desktop/ visualization instances) and restart them.


## To view and save the map as a point cloud
The `cloud_map` topic can be viewed via RVIZ; this is the recommended way to view it, however if you’re running rtabmap on a remote sensor like the Trisect and sending the point cloud data over the network, it will grow in size (see Metrics and Results).


### Saving cloud_map
The easiest way to save the point cloud is using the Database Viewer, as specified below.

Alternatively, you can run `rosrun pcl_ros pointcloud_to_pcd input:=/rtabmap/cloud_map`. If rtabmap is not running (e.g., paused), call this to republish the map after being subscribed to `/cloud_map`: `rosservice call /rtabmap/publish_map 1 1 0`.

You could also use the standalone rtabmap_viz UI to regenerate clouds using the export dialog: File -> Export 3D Clouds.

### Constructing map over-network with map_assembler
If you would like to maintain as low and consistent of a network load as possible (see Metrics and Results), you would want to run rtabmap's map assembler node to reconstruct the cloud on the client/ desktop side. Follow the steps above to run rtabmap on the Trisect. On the desktop, run another instance of rtabmap_docker, but instead of doing `roslaunch rtabmap_depth.launch`, intead do `map_assembler.launch`. This will only subscribe to the final `mapData` result and will replace `cloud_map` with a locally-generated version.

### Using databaseViewer
You save the final point cloud using [RTAB-Map's Database Viewer](https://github.com/introlab/rtabmap/wiki/Tools). To get the database file out of the docker container:
1. Find the container ID using `docker ps`
2. `docker cp {container ID here}:/root/.ros/rtabmap.db .` The dot at the end means the database file will copy to your current directory.
3. `rtabmap-databaseViewer rtabmap.db`

There's a lot more you can do with this tool than save the point cloud!

# Configuration
## Relevant Parameters
Some within [Advanced Parameter Tuning](http://wiki.ros.org/rtabmap_ros/Tutorials/Advanced%20Parameter%20Tuning) may be helpful! You can find a full list of parameters in [Parameters.h](https://github.com/introlab/rtabmap/blob/master/corelib/include/rtabmap/core/Parameters.h#L161). To find the actual parameter names in ROS, with their namespaces, do `rosparam list | grep NAME_OF_PARAMETER` and replace NAME_OF_PARAMETER with the one of interest. You can find examples of setting these parameters in [the launch file.](launch/rtabmap_depth.launch). You can also set them live through the command line with `rosparam set` or through the rtabmapviz interface in Window -> Preferences -> Advanced Settings. There are many settings to tweak with here, and we've found the ones below/ in the launch file to be the most relevant thus far.


### Robust Odometry
* ResetCountdown: "Automatically reset odometry after X consecutive images on which odometry cannot be computed (value=0 disables auto-reset)."
* StartNewMapOnLoopClosure: "Start a new map only if there is a global loop closure with a previous map."

### Point cloud downsampling
* CellSize: "Resolution of the occupancy grid."
* NoiseFilteringMinNeighbors: "Noise filtering minimum neighbors."
* NoiseFilteringRadius: "Noise filtering radius (0=disabled). Done after segmentation."

# Metrics and Results
## Accuracy

TODO - figure out point cloud evaluation goals before proceeding.

## Performance
The conclusion from the following results is that if you want a lower computational load on the sensor (~25% of CPU) and are fine with a higher (12% of Gigabit) network load, use configuration A. Otherwise, if you want a higher computational load on the sensor (~50%) and a much lower network load, use configurations B or C: B is fine for smaller maps which don't increase much in size, but C has the most consistent and low bandwidth.

### Network Load
<div align="center">
<img src="images/bw_description.png" align="center" width="1000" alt="Description of BWs">
</div>
<div align="center">
<img src="images/bw_1.png" align="center" width="500" alt="BW of configurations A, B, and C">
<img src="images/bw_2.png" align="center" width="500" alt="BW of configurations B and C">
</div>

### Computational Load
<div align="center">
<img src="images/cpu_usage.png" align="center" width="500" alt="Bar graph of CPU usage">
</div>

# Troubleshooting

# References and Resources
* [Our rtabmap_docker gitlab](https://gitlab.com/rsa-perception-sensor/rtabmap_docker)
* [ROS Answers for rtabmap_ros](https://answers.ros.org/questions/scope:all/sort:activity-desc/tags:rtabmap/page:1/)
* [rtabmap_ros wiki](http://wiki.ros.org/rtabmap_ros)
    * [rtabmap_slam wiki](http://wiki.ros.org/rtabmap_slam): contains list of arguments, subscribed topics, published topics, services, parameters, required tf transforms, and provided tf transforms for the main node
* [rtabmap_ros github](https://github.com/introlab/rtabmap_ros)
    * [Docker images](https://github.com/introlab/rtabmap_ros/tree/master/docker)
* [RTAB-Map home page](http://introlab.github.io/rtabmap/)

# Appendix
